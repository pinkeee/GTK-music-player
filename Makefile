TARGET = out

CXX=gcc
# debug
DEBUG=-g
# optimisation
OPT=-O0
# warnings
WARN=-Wall

PTHREAD=-pthread

FLAGS = -Os -I.. -L.. -L x64 -lbass -lm -Wl,--no-warn-search-mismatch,-rpath,'$$ORIGIN/..:$$ORIGIN/../x64'

CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) 

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

# linker
LD=g++
LDFLAGS=$(PTHREAD) $(GTKLIB) -export-dynamic 

OBJS=    main.o 

all: $(OBJS)
	$(LD) $(FLAGS) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o: src/main.c src/bass.h
	$(CC) -c $(CCFLAGS) src/main.c $(GTKLIB) 
	
	
clean:
	rm -f *.o $(TARGET)
