#include <gtk/gtk.h>
#include "bass.h"

#define SIZE 126
#define SIZE_SONG 64
#define FORWARD 869428

typedef struct {
    GtkWindow *window;
    GtkWidget *window_widget;
       
    GtkWidget *btn_forwards;
    GtkWidget *btn_play;
    GtkWidget *btn_backwards;
    GtkWidget *btn_mute;
    GtkWidget *lbl_title;
    GtkWidget *lbl_time;
    GtkWidget *lbl_error;
    GtkWidget *file_choose;
    GtkWidget *main_dialog;

    GtkAdjustment *adjustment1;

} App_widgets;

static gchar *name = {0};
static gchar buf[SIZE] = {0};
static gchar *name_buf[SIZE] = {0};
static gchar *song_buffer[SIZE] = {0};

static unsigned int count = -1;
static unsigned int mute = -1;
static unsigned int current = 0;
static unsigned int time_length = 0;
static unsigned int pos = 0.0;

static gdouble slider_val = 0.5;

char *strrchr(const char *string, int c);

DWORD chan;

int main(int argc, char *argv[]) {
    GtkBuilder      *builder; 
    App_widgets     *widgets = g_slice_new(App_widgets);

    gtk_init(&argc, &argv);

    if (HIWORD(BASS_GetVersion()) != BASSVERSION) {
		g_print("An incorrect version of BASS was loaded");
		return 0;
	}
	// initialize BASS
	if (!BASS_Init(-1, 44100, 0, NULL, NULL)) {
		g_print("Can't initialize device");
		return 0;
	}

    pos = BASS_ChannelGetPosition(chan, 0);

    builder = gtk_builder_new_from_file("glade/window_main.glade");

    widgets -> window = GTK_WINDOW(gtk_builder_get_object(builder, "window_main"));
    widgets -> window_widget = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));

    // pointers to widgets
    widgets -> btn_forwards = GTK_WIDGET(gtk_builder_get_object(builder, "btn_forwards"));
    widgets -> btn_play = GTK_WIDGET(gtk_builder_get_object(builder, "btn_play"));
    widgets -> btn_backwards = GTK_WIDGET(gtk_builder_get_object(builder, "btn_backwards"));
    widgets -> btn_mute = GTK_WIDGET(gtk_builder_get_object(builder, "btn_mute"));
    widgets -> lbl_title = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_title"));
    widgets -> lbl_time = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_time"));
    widgets -> lbl_error = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_error"));
    widgets -> file_choose = GTK_WIDGET(gtk_builder_get_object(builder, "file_choose"));
    widgets -> main_dialog = GTK_WIDGET(gtk_builder_get_object(builder, "main_dialog"));

    gtk_builder_connect_signals(builder, widgets);

    g_object_unref(builder);
    gtk_widget_show_all(widgets -> window_widget);
    
    gtk_main();

    BASS_Free();
    g_slice_free(App_widgets, widgets);

    return EXIT_SUCCESS;
}

void on_btn_help_activate(GtkButton *btn_help, App_widgets *wdgts) {
    gtk_dialog_run(GTK_DIALOG(wdgts -> main_dialog));
    gtk_widget_hide(wdgts -> main_dialog);
}

void on_window_main_destroy(void) {
    gtk_main_quit();
}

void on_btn_quit_activate(void) {
    gtk_main_quit();
}

void on_btn_mute_clicked(void) {
    mute++;
    
    switch (mute %2 == 0) {
        case TRUE: 
            g_print("[*] Mutting audio\n");
            BASS_SetVolume(0);

            break;
        case FALSE: 
            g_print("[*] Unmutting audio\n");
            BASS_SetVolume(slider_val);

            break;
    }
}

void find_title(gchar *name) {
    strcpy(buf, name);
    gchar *ptr = NULL;
    ptr = (gchar*) malloc(strlen(name));

    ptr=strrchr(buf, '/');
    
    if (ptr[0] == '/') ptr++;
    strcpy(buf, ptr);

    g_print("[*] Title: %s    \n[*] Dir: %s\n", buf, name);

    name_buf[strlen((gchar *)name_buf)] = (gchar*) malloc(sizeof(*ptr)+1);
    snprintf((gchar *)name_buf, strlen(name)+1, "%s" , name);

    g_free(name);
}   

void need_reset(void) {
    enum vals {p=-1};

    BASS_ChannelSetPosition(chan, 0, 0);
    BASS_Stop();

    pos = p;
    current = p;
    count = p;
    
    snprintf((gchar *)name_buf, 1, "%s" , "");
    snprintf((gchar *)song_buffer, 1, "%s" , "");
}

void on_btn_open_activate(GtkButton *btn_open, App_widgets *wdgts) {
    gtk_widget_show(wdgts -> file_choose);

    if (gtk_dialog_run(GTK_DIALOG(wdgts -> file_choose)) == GTK_RESPONSE_OK) {
        // gets dir and file name (with type)
        name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(wdgts -> file_choose));
        switch (name != NULL) {
            case TRUE: 
                need_reset();
                find_title(name);

                break;
            case FALSE: 
                g_print("[*] Error\n");

                break;
        }
    }
    gtk_widget_hide(wdgts -> file_choose); 
    gtk_label_set_text(GTK_LABEL(wdgts -> lbl_title), buf);
    gtk_label_set_text(GTK_LABEL(wdgts -> lbl_error), "");
}

void get_song_length() {
    register unsigned int test_len_by = BASS_ChannelGetLength(chan, BASS_POS_BYTE);
    register unsigned int sec, min;
    time_length = BASS_ChannelBytes2Seconds(chan, test_len_by);

    sec = time_length % 60;
    min = time_length / 60;

    switch (sec < 10) {
        case TRUE: 
            song_buffer[strlen((gchar *)song_buffer)] = (gchar*) malloc(sizeof(sec + min)+2);
            sprintf((gchar *)song_buffer, "%d%s%d%d", min,":",0,sec);
        
            g_print("[*] Song Length: %s\n", (gchar *)song_buffer);
            break;
        case FALSE:
            song_buffer[strlen((gchar *)song_buffer)] = (gchar*) malloc(sizeof(sec + min)+1);
            sprintf((gchar *)song_buffer, "%d%s%d", min,":",sec);

            g_print("[*] Song Length: %s\n", (gchar *)song_buffer);
            break;
    }
}

void on_btn_play_clicked(GtkButton *btn_play, App_widgets *wdgts) {
    count++;
    current = BASS_ChannelGetPosition(chan, BASS_POS_BYTE);

    if (name_buf != NULL && count %2 == 0) {
        switch (!(chan = BASS_StreamCreateFile(FALSE, name_buf, 0, 0, BASS_STREAM_PRESCAN))
            && !(chan = BASS_MusicLoad(FALSE, name_buf, 0, 0, BASS_MUSIC_RAMP | BASS_STREAM_PRESCAN, 1))) {
            
                case TRUE: 
                    g_print("Can't play file\n");

                    gtk_label_set_markup(GTK_LABEL(wdgts -> lbl_error), "<span foreground='red' weight='bold' font='8'>Stopped</span>"); 
		    break;
                case FALSE: 
                    BASS_Start();
                    BASS_ChannelSetPosition(chan, 0, 0);
                    BASS_ChannelPlay(chan, FALSE);
                    BASS_SetVolume(slider_val);
                    
                    get_song_length();

		    gtk_label_set_markup(GTK_LABEL(wdgts -> lbl_error), "<span foreground='green' weight='bold' font='8'>Playing Song...</span>");
                    gtk_button_set_label(GTK_BUTTON(wdgts -> btn_play), "⏸️");
                    
                    gtk_label_set_text(GTK_LABEL(wdgts -> lbl_time), (gchar *)song_buffer);

                    BASS_ChannelSetPosition(chan, current, 0);
                    pos = BASS_ChannelGetPosition(chan, 0);

                    gtk_window_set_title(wdgts -> window, buf);

                    break;
        }
    } else {
        g_print("[*] Stopped music\n");

	gtk_label_set_markup(GTK_LABEL(wdgts -> lbl_error), "<span foreground='red' weight='bold' font='8'>Stopped</span>");
        gtk_button_set_label(GTK_BUTTON(wdgts -> btn_play), "▶️");

        current = BASS_ChannelGetPosition(chan, BASS_POS_BYTE);
        gtk_window_set_title(wdgts -> window, "Paused");

        BASS_Stop();
    }
}

void on_sc_vol_value_changed(GtkRange *range, gpointer user_data) {
   slider_val = gtk_range_get_value(range);

   BASS_SetVolume(slider_val);
}

void on_btn_backwards_clicked() {
    pos = BASS_ChannelGetPosition(chan, 0);

    BASS_ChannelSetPosition(chan, pos-FORWARD, 0);

    //g_print("[*] Backwards 5 seconds\n");
}

// moves forwards 5 seconds in the song
void on_btn_forwards_clicked(GtkButton *btn_forwards, App_widgets *wdgts) {
    pos = BASS_ChannelGetPosition(chan, 0);

    BASS_ChannelSetPosition(chan, pos+FORWARD, 0);

    //g_print("[*] Skipped 5 seconds\n");
}
