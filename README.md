# GTK-music-player
A basic Music player written in C using GTK

# Building 
Install dependencies, move libbass.so to /usr/lib (on Arch) and run 'make' from inside the dir

```
$ sudo pacman -S gtk3
$ sudo pacman -S glade
$ cd dir/to/clone
$ sudo mv libbass.so /usr/lib
$ make
```
After it has compiled run:

```
$ ./out
```

# Images

![alt text](https://raw.githubusercontent.com/3vasion/GTK-music-player/master/images/Screenshot1.png)

![alt text](https://raw.githubusercontent.com/3vasion/GTK-music-player/master/images/Screenshot2.png)
